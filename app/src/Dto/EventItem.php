<?php

namespace Maaaxim\Dto;

/**
 * Class EventItem
 * @package Maaaxim\Dto
 */
class EventItem
{
    /**
     * @var
     */
    protected $priority;

    /**
     * @var
     */
    protected $event;

    /**
     * @var array
     */
    protected $conditions = [];

    /**
     * EventItem constructor.
     * @param $event
     * @param $conditions
     * @param $priority
     */
    public function __construct($event, $conditions, $priority)
    {
        $this->event = $event;
        $this->conditions = $conditions;
        $this->priority = $priority;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @return array
     */
    public function getConditions(): array
    {
        return $this->conditions;
    }
}