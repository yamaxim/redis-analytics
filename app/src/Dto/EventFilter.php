<?php

namespace Maaaxim\Dto;

use Exception;

/**
 * Class EventFilter
 * @package Maaaxim\Dto
 */
class EventFilter
{
    /**
     * @var array
     */
    protected $params = [];

    /**
     * EventFilter constructor.
     * @param array $params
     * @throws Exception
     */
    public function __construct(array $params)
    {
        if(empty($params)){
            throw new Exception("Set params please");
        }
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}