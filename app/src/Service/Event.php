<?php

namespace Maaaxim\Service;

use Exception;
use Maaaxim\Dto\EventFilter;
use Maaaxim\Dto\EventItem;
use Predis\Client;

/**
 * Event service
 *
 * @package Maaaxim\Service
 */
class Event
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * Event constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Proceed user's request
     *
     * @param EventFilter $filter
     * @return int
     * @throws Exception
     */
    public function getEvent(EventFilter $filter): int
    {
        $plainArray = [];
        foreach($filter->getParams() as $key => $value){
            $plainArray[] = $key;
            $plainArray[] = $value;
        }
        $this->client->getProfile()->defineCommand("sendKwargs", "Maaaxim\Command\MultipleParamSearchCommand");
        $found = $this->client->sendKwargs("events", $plainArray);

        // get range
        // ZSCORE events:sort N
        foreach($found as $event){
            $score = $this->client->zscore("events:sort", $event);
            $foundSorted[$score] = $event;
        }

        ksort($foundSorted);
        $eventId = reset($foundSorted);
        if($eventId < 0){
            throw new Exception("Event not found");
        }
        return $eventId;
    }

    /**
     * Add new event
     * (store params in hashes and sort in sorted sets)
     *
     * @param EventItem $eventData
     */
    public function add(EventItem $eventData): void
    {
        // get id
        $key = $this->client->incr('events:increment');

        // add params
        $parameters = $eventData->getConditions();
        foreach($parameters as $parameter => $value){
            $this->client->hSet("events:{$key}", $parameter, $value);
        }

        // add sort
        $sort = $eventData->getPriority();
        $this->client->zAdd('events:sort', $sort, $key);
    }

    /**
     * Clear all events
     */
    public function clear(): void
    {
        $this->client->eval(luaDropKeys(), 1, "events:*");
    }
}