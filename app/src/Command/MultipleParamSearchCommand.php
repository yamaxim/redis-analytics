<?php

namespace Maaaxim\Command;

use Predis\Command\ScriptCommand;

/**
 * @package Maaaxim\Command
 */
class MultipleParamSearchCommand extends ScriptCommand
{
    /**
     * @return int
     */
    public function getKeysCount(): int
    {
        return 1;
    }

    /**
     * @return string
     */
    public function getScript(): string
    {
        return <<<LUA
        
local rows_to_run = redis.call('GET', KEYS[1] .. ':increment')
local params_count = 0
local found = {}

-- set parameter count
for _ in pairs(ARGV) do
    params_count = params_count + 1
end
params_count = params_count / 2

-- run rows
for i = 1, rows_to_run do
    
    local param_match_count = 0
    local param_values = redis.call('HGETALL', KEYS[1] .. ':' .. i)
    
    -- run row's parameter values
    for index in pairs(param_values) do
    
        -- run logic only on keys
        if not (index % 2 == 0) then
                
            -- run sent params
            for subindex in pairs(ARGV) do
            
                -- compare with sent parameters
                if not (subindex % 2 == 0) then
                
                    if ARGV[subindex] == param_values[index] and ARGV[subindex+1] == param_values[index+1] then
                        param_match_count = param_match_count + 1
                    end
                end
            end
        end
    end
    
    -- add only matched rows
    if param_match_count == params_count then
        table.insert(found, i)
    end
end

return found
LUA;
    }

    /**
     * @param array $arguments
     * @return array
     */
    public function filterArguments(array $arguments): array
    {
        $argv = array_pop($arguments);
        $arguments = parent::filterArguments(array_merge($arguments, $argv));
        return $arguments;
    }
}