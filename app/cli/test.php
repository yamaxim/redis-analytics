<?php

use Maaaxim\Dto\EventFilter;
use Maaaxim\Dto\EventItem;
use Maaaxim\Service\Event;
use Predis\Client;

require_once "./vendor/autoload.php";

/**
 * Инкремент событий
 * SET events:id 0
 * INCR events:id
 *
 * HSET events:1 param1 1
 * HSET events:2 param1 2 param2 2
 * HSET events:3 param1 1 param2 2
 *
 * ZADD events:sort 1000 1
 * ZADD events:sort 2000 2
 * ZADD events:sort 3000 3
 *
 *
 * Бежим по списку и получаем по параметрам наши события
 * https://stackoverflow.com/questions/45558708/iterating-over-redis-hash-keys-with-a-wild-card-search
 *
 * HGET events:1
 *
 * Потом их находим в sorted set
 */

$client = new Client(['host'   => 'redis']);
$event = new Event($client);

//$event->add(new EventItem(
//    "click",
//    ["param1" => 1],
//    1000
//));
//
//$event->add(new EventItem(
//    "hover",
//    ["param1" => 2, "param2" => 2],
//    2000
//));
//
//$event->add(new EventItem(
//    "focus",
//    ["param1" => 1, "param2" => 2],
//    3000
//));

$foundEvent = $event->getEvent(new EventFilter(
    [
        "param1" => 1,
        "param2" => 2
    ]
));

var_dump($foundEvent);
echo PHP_EOL;

//$event->clear();