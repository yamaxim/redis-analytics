<?php

/**
 * @return string
 */
function luaDropKeys(): string
{
$lua = <<<LUA
local k = 0
for i, name in ipairs(redis.call('KEYS', KEYS[1]))
do
    redis.call('DEL', name)
    k = k+1
end
return k
LUA;
return $lua;
}